class Cart {

    constructor(contents,totalAmount){
        this.contents = [];
        this.totalAmount = totalAmount;
    }
    addToCart(product,quantity){
        this.contents.push({product,quantity});
        
        return this;
    }
    showCartContents(){
        this.contents.length;
        console.log(this.contents)
        return this;
    }
    clearCartContents(){
        this.contents.length = [];
        return this;
    }
    computeTotal(){
        let totalAmt = 0;
        this.contents.forEach(content => {
        let price = content.product.price;
        let quantity = content.quantity;
        let subTotal = price * quantity;
        
        totalAmt += subTotal
        
        console.log(totalAmt)

        })

        this.totalAmount = totalAmt

        return this; 

    }
}

let cart01 = new Cart([],0);


class Product{
    constructor(name,price){
        this.name = name;
        this.price = price;
        this.isActive = true;

        if(typeof price === "number"){
			this.price = price;
		}
	
    }
    archive(){
        this.isActive = false;
        return this;
    }
    updatePrice(newPrice){
        this.price = newPrice;
        return this;
    }
}

let prod1 = new Product("shampoo",10);
let prod2 = new Product("soap",15);

class Customer{

    constructor(email,cart,orders){
        this.email = email;
        this.cart = new Cart;
        this.orders = [];

    }

    checkOut(){
        if(this.cart.contents.length > 0){
            this.orders.push({
                products: this.cart.contents,
                totalAmount: this.cart.computeTotal().totalAmount
            })
        }
        return this
    }
}

let john = new Customer("john@mail.com");

john.cart.addToCart(prod1,1);
john.cart.addToCart(prod2,3);

john.checkOut();

